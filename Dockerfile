FROM ubuntu:18.04
ARG TARGETARCH
COPY start_metricbeat /start_metricbeat
COPY metricbeat-oss-8.0.0-${TARGETARCH}.deb /metricbeat-oss-8.0.0-${TARGETARCH}.deb
RUN dpkg -i metricbeat-oss-8.0.0-${TARGETARCH}.deb && rm metricbeat-oss-8.0.0-${TARGETARCH}.deb
ENTRYPOINT [ "/usr/share/metricbeat/bin/metricbeat" ]
